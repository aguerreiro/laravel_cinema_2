@extends('layouts.app')
@section('title', 'Edit a movie')
@section('content')

<main>
    <div class="container">
        <h1 class="display-6" style="text-align: center; margin-top: 50px;">Edit a movie</h1>

        <form method="POST" action="{{ route('movie.update', $movie->id) }}">

            {{ csrf_field() }}
            {{method_field('PUT')}}
            
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control form-control-sm" value="{{ $movie->title }}" required />
            </div>

            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" name="year" id="year" class="form-control form-control-sm" value="{{ $movie->year }}" required />
            </div>
            <div class="form-group">
                <select class="custom-select" name="artist_id" required>
                    @foreach($artists as $artist)
                        <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Modify</button>
        </form>
    </div>
</main>
@endsection
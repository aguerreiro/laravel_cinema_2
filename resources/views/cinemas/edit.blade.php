@extends('layouts.app')
@section('title', 'Edit cinema')
@section('content')

<main>
    <div class="container">
        <h1 class="display-1" style="text-align: center; margin-top: 50px;">Edit a cinema</h1>

        <form method="POST" action="{{ route('cinema.update', $cinema->id) }}">

            {{ csrf_field() }}
            {{method_field('PUT')}}

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control form-control-sm"
                    value="{{ $cinema->name }}" required />
            </div>

            <div class="form-group">
                <label for="street">Street</label>
                <input type="text" name="street" id="street" class="form-control form-control-sm"
                    value="{{ $cinema->street }}" required />
            </div>
            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="number" name="postcode" id="postcode" class="form-control form-control-sm"
                    value="{{ $cinema->postcode }}" required />
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" class="form-control form-control-sm"
                    value="{{ $cinema->city }}" required />
            </div>
            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" class="form-control form-control-sm"
                    value="{{ $cinema->country }}" required />
            </div>

            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Modify</button>
        </form>
    </div>
</main>
@endsection
@extends('layouts.app')
@section('title', 'Edit an artist')
@section('content')


<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin-top: 50px;">Edit an artist</h1>
        
        <form method="POST" action="{{ route('artist.update', $artist->id) }}">
            {{ csrf_field() }}
            {{method_field('PUT')}}

            <div class="form-group">
                <label for="firstname">First name</label>
                <input type="text" name="firstname" id="firstname" class="form-control form-control-sm"
                    value="{{ $artist->firstname }}" required />
            </div>
            <div class="form-group">
                <label for="name"> Last Name</label>
                <input type="text" name="name" id="name" class="form-control form-control-sm"
                    value="{{ $artist->name }}" required />
            </div>
            <div class="form-group">
                <label for="birthdate">Birthdate</label>
                <input type="number" name="birthdate" id="birthdate" class="form-control form-control-sm" value="{{ $artist->birthdate }}" required />
            </div>
            <div class="form-group">
                <label for="photo">Artist photo</label>
                <input class="form-control form-control-file form-control-sm" type="file" name="photo" id="photo">
            </div>

            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Modify</button>
        </form>
    </div>
</main>
@endsection
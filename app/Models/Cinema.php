<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $fillable = [
        'name', 'street', 'postcode', 'city', 'country'
    ];

   // Liaison one_to_many avec la table room (Un cinéma gère plusieurs salles)
    public function manages(){
		return $this->hasMany('App\Models\Room');
	}
}

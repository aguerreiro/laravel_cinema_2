<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([[
            'title' => 'Avatar',
            'year' => '2009',
            'artist_id' => '4',
        ],[
            'name' => 'E.T.',
            'year' => '1982',
            'artist_id' => '5',
        ],[
            'name' => 'Teminator',
            'year' => '1984',
            'artist_id' => '4',
        ],[
            'name' => 'Terminator 2',
            'year' => '1991',
            'artist_id' => '4',
        ],[
            'name' => 'Terminator 2',
            'year' => '1991',
            'artist_id' => '4',
        ],[
            'name' => 'Batman 1 - Batman Begins ',
            'year' => '2005',
            'artist_id' => '4',
        ],[
            'name' => 'Batman 2 - The Dark Knight',
            'year' => '2008',
            'artist_id' => '4',
        ],[
            'name' => 'Batman 3 - The Dark Knight Rises',
            'year' => '2012',
            'artist_id' => '4',
        ],[
            'name' => 'Mon voisin Totoro',
            'year' => '1988',
            'artist_id' => '7',
        ],[
            'name' => '1917',
            'year' => '2019',
            'artist_id' => '6',
        ]]);
    }
}

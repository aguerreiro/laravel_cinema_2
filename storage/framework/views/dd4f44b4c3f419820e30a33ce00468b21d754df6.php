<?php $__env->startSection('title', 'Edit a movie'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="container">
        <h1 class="display-6" style="text-align: center; margin-top: 50px;">Edit a movie</h1>

        <form method="POST" action="<?php echo e(route('movie.update', $movie->id)); ?>">

            <?php echo e(csrf_field()); ?>

            <?php echo e(method_field('PUT')); ?>

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control form-control-sm" value="<?php echo e($movie->title); ?>" required />
            </div>

            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" name="year" id="year" class="form-control form-control-sm" value="<?php echo e($movie->year); ?>" required />
            </div>
            <div class="form-group">
                <select class="custom-select" name="artist_id" required>
                    <?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Modify</button>
        </form>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/movies/edit.blade.php ENDPATH**/ ?>
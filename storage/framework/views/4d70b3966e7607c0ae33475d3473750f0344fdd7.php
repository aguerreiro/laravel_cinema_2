<?php $__env->startSection('title', 'Add new cinema'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin-top: 50px;">Add new cinema</h1>
        
        <form method="POST" action="<?php echo e(route('cinema.store')); ?>">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control form-control-sm" value="" required />
            </div>

            <div class="form-group">
                <label for="street">Street</label>
                <input type="text" name="stret" id="street" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="number" name="postcode" id="postcode" value="" class="form-control form-control-sm"
                    required />
            </div>

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" value="" class="form-control form-control-sm" required />
            </div>
            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Create</button>
        </form>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/cinemas/create.blade.php ENDPATH**/ ?>